- Install Yarn to manage dependencies:
```
brew install yarn
```

- Use Yarn to install dependencies, be sure to be in test1Germinal folder:
```
yarn
```

- First time, launch checkboard:
```
yarn run start
```

- launch the command to get JWT:

```curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"password":"germinal", "username":"germinal"}' \
  http://localhost:8000/login
```

- Request JSON event with the command (place JWT from previous response)

```curl -X GET \
  -H 'Authorization: Bearer (place JWT from previous response)' \
  http://localhost:8000
```
