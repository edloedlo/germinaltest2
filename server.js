const express = require('express');
const bodyParser = require('body-parser');
let jwt = require('jsonwebtoken');
let config = require('./config');
let middleware = require('./middleware');

class HandlerGenerator {
  login (req, res) {
    let username = req.body.username;
    let password = req.body.password;
    let mockedUsername = 'germinal';
    let mockedPassword = 'germinal';

    if (username && password) {
      if (username === mockedUsername && password === mockedPassword) {
        let token = jwt.sign({username: username},
          config.secret,
          { expiresIn: '24h'
          }
        );
        res.json({
          success: true,
          message: 'Authentication successful!',
          token: token
        });
      } else {
        res.send(403).json({
          success: false,
          message: 'Incorrect username or password'
        });
      }
    } else {
      res.send(400).json({
        success: false,
        message: 'Authentication failed! Please check the request'
      });
    }
  }
  event (req, res) {
    res.send({
      event_name: "Recrutement développeur",
      start_time: "01/01/2020 10:12",
      end_time: "02/01/2020 17:30",
      cover: "https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/70427152_141014303796807_4689528362182377472_o.jpg?_nc_cat=101&_nc_oc=AQl47NhhyfGAdvqBsH7Ht14PAAlL6Ync2zfzAyY4B-fA65fU2G4ASZMS3Z55bZxRFYc&_nc_ht=scontent.xx&oh=56bda3ef0e9af6f7e79c50405a306295&oe=5DF61DDE",
      days_before_token_expiration: "59"
    });
  }
}

function main () {
  let app = express();
  let handlers = new HandlerGenerator();
  const port = process.env.PORT || 8000;
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.post('/login', handlers.login);
  app.get('/', middleware.checkToken, handlers.event);
  app.listen(port, () => console.log(`Server is listening on port: ${port}`));
}

main();
